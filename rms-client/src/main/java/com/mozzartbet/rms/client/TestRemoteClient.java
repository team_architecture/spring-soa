package com.mozzartbet.rms.client;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mozzartbet.rms.common.domain.Team;
import com.mozzartbet.rms.common.service.TeamService;


public class TestRemoteClient {

	private TeamService teamService;
	private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);
	
	public static void main(String[] args) {
		System.out.println("Loading context ...");
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		final TestRemoteClient testClient = (TestRemoteClient) ctx.getBean("testRemoteClient");
		
		//calling RMI service in parallel
		for (int i = 0; i < 10; i++) {
			fixedThreadPool.submit(new Callable<Team>() {

				@Override
				public Team call() throws Exception {
					long start = System.currentTimeMillis();
					Team team = testClient.getTeamService().findTeam(Integer.valueOf(1));
					System.out.println(team);
					System.out.println("in:" + (System.currentTimeMillis() - start) + " millis.");
					return team;
				}
			});
		}
		
		fixedThreadPool.shutdown();
	}
	
	public TeamService getTeamService() {
		return teamService;
	}
	
	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}

}
