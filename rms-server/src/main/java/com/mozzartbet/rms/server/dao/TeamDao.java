package com.mozzartbet.rms.server.dao;

import java.util.List;

import com.mozzartbet.rms.common.domain.Team;

public interface TeamDao {

	void add(Team Team);

	Team findById(Integer id);

	List<Team> findAll();

	void update(Team team);

	void delete(Integer id);

}
