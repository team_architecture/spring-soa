package com.mozzartbet.rms.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mozzartbet.rms.common.domain.Team;
import com.mozzartbet.rms.common.service.TeamService;
import com.mozzartbet.rms.server.dao.TeamDao;

/**
 * Spring will give the bean a name created by lowercasing the first character
 * of the class and using the rest of the camel-cased name for the bean name.
 * Naming service with different name should be done @Service("myServiceName")
 */
@Service()
public class DefaultTeamService implements TeamService {

	/*
	 * If you are using Spring 2.5 or later, you can simply include the
	 * <context:annotation-config> element in your bean configuration file. It
	 * activates various annotations to be detected in bean classes: Spring's
	 * 
	 * @Required and @Autowired, as well as JSR 250's @PostConstruct,
	 * @PreDestroy and @Resource (if available) and JPA's @PersistenceContext
	 * and @PersistenceUnit (if available).
	 */	 
	@Autowired
	private TeamDao teamDao;
	
	/*By default, only unchecked exceptions (i.e., of type RuntimeException and Error) will cause a transaction 
	to roll back, while checked exceptions will not.*/ 
	@Transactional (readOnly = true)
	public List<Team> getTeamList() {
		List<Team> teamList = teamDao.findAll();
		return teamList;
	}

	/*
	 * This "rollbackFor" is for educational purpose. This transaction would
	 * rollback if a RuntimeException gets thrown from DAO.
	 */
	@Transactional(rollbackFor = Exception.class)
	public void addTeam(Team newTeam) {
		teamDao.add(newTeam);
	}

	@Transactional(readOnly = true)
	public Team findTeam(Integer id) {
		return teamDao.findById(id);
	}

	@Transactional
	public void updateTeam(Team team) {
		teamDao.update(team);
	}

	@Transactional
	public void deleteTeam(Integer id) {
		teamDao.delete(id);
	}

}
