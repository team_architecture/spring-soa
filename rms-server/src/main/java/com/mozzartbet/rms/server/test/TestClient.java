package com.mozzartbet.rms.server.test;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.mozzartbet.rms.common.domain.Team;
import com.mozzartbet.rms.common.service.TeamService;

@Component("testClient")
public class TestClient {
	private static final int TEAM_ID = 156;

	private static final Logger logger = LoggerFactory.getLogger(TestClient.class);

	@Autowired
	private TeamService teamService;

	public static void main(String[] args) {
		logger.info("Loading context ...");
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"data-access-config.xml");
		// load an instance of this class as a Spring managed bean
		TestClient jdbcIntro = (TestClient) ctx.getBean("testClient");
		jdbcIntro.runExamples();
	}

	private void runExamples() {
		// List of initial set of teams
		logger.info("\nList of initial set of teams:");
		List<Team> teamList = teamService.getTeamList();
		logger.info(teamList.toString());

		// Add a new team
		logger.info("\nAdd a new team:");
		Team newteam = new Team();
		newteam.setId(TEAM_ID);
		newteam.setName("Partizan");
		teamService.addTeam(newteam);
		logger.info("team " + newteam.getName()
				+ " added with id: " + newteam.getId());
		teamList = teamService.getTeamList();
		logger.info(teamList.toString());

		// Update a team
		logger.info("\nUpdate a team:");
		Team team = teamService.findTeam(TEAM_ID);		
		teamService.updateTeam(team);
		logger.info("team " + team.getName());
		teamList = teamService.getTeamList();
		logger.info(teamList.toString());

		// Delete a team
		logger.info("\nDelete a team:");
		teamService.deleteTeam(TEAM_ID);
		logger.info("team with id " + TEAM_ID + " deleted");
		teamList = teamService.getTeamList();
		logger.info(teamList.toString());
	}

}
