package com.mozzartbet.rms.server.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestRMIServer {
	private static final Logger LOG = LoggerFactory.getLogger(TestRMIServer.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LOG.info("Loading context ...");
		new ClassPathXmlApplicationContext("rmi-server.xml");
		LOG.info("Server started");
	}

}
