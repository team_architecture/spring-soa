package com.mozzartbet.rms.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mozzartbet.rms.common.domain.Team;

@Repository
public class SimpleJdbcTeamDao implements TeamDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(SimpleJdbcTeamDao.class); 

	private NamedParameterJdbcTemplate jdbcTemplate;

	/**
	 * The DataSource is injected and used to create a SimpleJdbcTemplate. The
	 * JdbcTemplate class is designed to be thread-safe, so you can declare a
	 * single instance of it in the IoC container and inject this instance into
	 * all your DAO instances.
	 * 
	 * @param dataSource
	 */
	@Autowired
	public void init(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public void add(Team team) {
		jdbcTemplate.update("insert into team (id, name) values(:id, :name)",
				new MapSqlParameterSource("id", team.getId()).addValue("name",
						team.getName()));

	}

	@Override
	public Team findById(Integer id) {
		try {
			/*
			 * The "queryForObject" will retrieve a single object of the type
			 * returned by the RowMapper implementation. If no team is found an
			 * exception will be thrown.
			 */
			return jdbcTemplate.queryForObject(
					"select id, name from team where id = :id",
					new MapSqlParameterSource("id", id), new TeamRowMapper());
		} catch (DataAccessException e) {
			//read: http://www.coderanch.com/t/59851/oa/Exception-handling-Spring
			LOG.info("No result found for id:{}", id);
			return null;
		}
	}

	@Override
	public List<Team> findAll() {
		return jdbcTemplate.query("select id, name from team",
				new MapSqlParameterSource(), new TeamRowMapper());
	}

	@Override
	public void update(Team team) {
		jdbcTemplate.update("update team set name = :name where id = :id",
				new MapSqlParameterSource("id", team.getId()).addValue("name",
						team.getName()));
	}

	@Override
	public void delete(Integer id) {
		jdbcTemplate.update("delete from team where id = :id",
				new MapSqlParameterSource("id", id));
	}

	/**
	 * Custom implementation of the RowMapper interface returning a Team object
	 * populated with data retrieved from the database row.
	 */
	private class TeamRowMapper implements RowMapper<Team> {

		public Team mapRow(ResultSet rs, int rowNum) throws SQLException {
			Team team = new Team();
			team.setId(rs.getInt("id"));
			team.setName(rs.getString("name"));
			return team;
		}

	}

}
