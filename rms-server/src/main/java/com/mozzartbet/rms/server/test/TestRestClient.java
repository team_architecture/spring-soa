package com.mozzartbet.rms.server.test;

import java.net.URISyntaxException;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.mozzartbet.rms.common.domain.Team;

public class TestRestClient {

	private static final String TEAM_URL = "http://localhost:8080/rms-server/webresources/teams/";

	public static void main(String[] args) throws RestClientException,
			URISyntaxException {
		TestRestClient client = new TestRestClient();

		System.out.println(client.getTeamList());
		System.out.println(client.findTeam(1));
		Team newTeam = new Team();
		newTeam.setId(123);
		newTeam.setName("Mozzart");
		System.out.println(client.addTeam(newTeam));
		System.out.println(client.deleteTeam(Integer.valueOf(123)));
	}

	public String getTeamList() {
		RestTemplate rt = new RestTemplate();
		String result = rt.getForObject(TEAM_URL, String.class);
		return result;
	}

	public String addTeam(Team newTeam) {
		RestTemplate rt = new RestTemplate();		
		String result = rt.postForObject(TEAM_URL, newTeam, String.class);
		return result;
	}

	public String findTeam(Integer id) {
		RestTemplate rt = new RestTemplate();
		String result = rt.getForObject(TEAM_URL + "{id}", String.class,
				Integer.valueOf(id));
		return result;
	}

	public void updateTeam(Team Team) {
	}

	public String deleteTeam(Integer id) {
		return "Deleted";
	};

}
