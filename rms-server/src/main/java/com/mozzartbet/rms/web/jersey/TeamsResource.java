
package com.mozzartbet.rms.web.jersey;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mozzartbet.rms.common.domain.Team;
import com.mozzartbet.rms.common.service.TeamService;

/**
 * Example resource class hosted at the URI path "/teams"<br>
 * This is "Root Resource Class" annotated with @Path that has at least one method
 * annotated with @Path or a resource method designator annotation such as @GET,
 * @PUT, @POST, @DELETE.
 */
@Component
@Path("/teams")
public class TeamsResource {
	
	@Autowired
	private TeamService teamService;
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     */
    @GET 
    @Produces("text/plain")
	public String getIt() {
		return "Hi there! In our database we can find these teams: "
				+ teamService.getTeamList();
	}
    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})    
    public List<Team> getTeamList() {
    	return teamService.getTeamList();
    }
    @Path("/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})    
    public Team findTeam(@PathParam("id") Integer id) {
    	Team team = teamService.findTeam(id);
    	if(team == null) {
			throw new WebApplicationException(Response.status(Status.NOT_FOUND)
					.type(MediaType.TEXT_PLAIN).entity("Team not found")
					.build());
    	}
		return team;
    }
    
    @POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addTeam(Team newTeam) {
    	teamService.addTeam(newTeam);
		return Response.status(Status.CREATED).type(MediaType.TEXT_PLAIN_TYPE)
				.entity("Team saved: " + newTeam).build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateTeam(Team team) {
    	teamService.updateTeam(team);
    	return Response.status(Status.OK).build();
    }
    
    @Path("/{id}")
    @DELETE
    public Response deleteTeam(@PathParam("id") Integer id) {
    	teamService.deleteTeam(id);
    	return Response.status(Status.OK).build();
    }
}
