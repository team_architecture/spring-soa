package com.mozzartbet.rms.common.service;

import java.util.List;

import com.mozzartbet.rms.common.domain.Team;


public interface TeamService {
	public List<Team> getTeamList();

	public void addTeam(Team newTeam);

	public Team findTeam(Integer id);

	public void updateTeam(Team Team);

	public void deleteTeam(Integer id);
}
