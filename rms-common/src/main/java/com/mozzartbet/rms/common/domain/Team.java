package com.mozzartbet.rms.common.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;
//XStreamAlias used for rms-web module and spring-xstream response
//XmlRootElement used for jersey xml response
@XStreamAlias("team")
@XmlRootElement
//If you want you can define the order in which the fields are written
//Optional
@XmlType(propOrder = { "id", "name"})
public class Team implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer id;	
	private String name;
		
	public void setId(Integer id) {
		this.id = id;
	}
	
	@XmlAttribute(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 *XmlElement annotation is for educational purposes :) 
	 */
	@XmlElement(name="team-name")
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "ID: " + id + " Name: " + name;
	}

}
