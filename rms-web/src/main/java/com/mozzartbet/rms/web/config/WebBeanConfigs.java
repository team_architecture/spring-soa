package com.mozzartbet.rms.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.web.servlet.view.xml.MarshallingView;


@Configuration
public class WebBeanConfigs {

	
	@Bean (name = "teamXmlView")
	public MarshallingView getTeamXmlViewer() {
		MarshallingView res = new MarshallingView();
		XStreamMarshaller result = new XStreamMarshaller();
		result.setAutodetectAnnotations(true);
		res.setMarshaller(result);
		return res;		
	}
}
