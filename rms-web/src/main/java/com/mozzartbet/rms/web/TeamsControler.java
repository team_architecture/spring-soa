package com.mozzartbet.rms.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mozzartbet.rms.common.domain.Team;
import com.mozzartbet.rms.common.service.TeamService;


@Controller
public class TeamsControler {

	@Autowired
	private TeamService teamService;
	
	@RequestMapping(value = "/teams/**", method = RequestMethod.GET)
	public ModelAndView getTeams() {
		List<Team> teams = teamService.getTeamList();
		ModelAndView mav = new ModelAndView("teamXmlView", BindingResult.MODEL_KEY_PREFIX + "teams", teams);
		return mav;		
	}
	
	@RequestMapping(value = "/teams/{id}", method = RequestMethod.GET)
	public ModelAndView getTeambyId(@PathVariable Integer id) {
		Assert.notNull(id, "Id must not be null");
		Team teams = teamService.findTeam(id);
		ModelAndView mav = new ModelAndView("teamXmlView", BindingResult.MODEL_KEY_PREFIX + "teams", teams);
		return mav;
		
	}
	
	@RequestMapping(value = "/teams/{id}", method = RequestMethod.DELETE)
	public ModelAndView deleteTeam(@PathVariable Integer id) {
		Assert.notNull(id, "Id must not be null");
		teamService.deleteTeam(id);
		return getTeams();
	}
	
}
